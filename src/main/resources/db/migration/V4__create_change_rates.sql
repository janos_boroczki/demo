CREATE TABLE public.change_rates
(
    id integer NOT NULL,
    from_symbol integer NOT NULL,
    to_symbol integer NOT NULL,
    rate numeric(10, 2) NOT NULL,
    PRIMARY KEY (id)
);

insert into public.change_rates (id, from_symbol, to_symbol, rate) values (nextval('hibernate_sequence'), 1, 2, 330);
