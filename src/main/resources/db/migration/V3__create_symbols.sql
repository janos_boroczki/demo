CREATE TABLE public.symbols
(
    id integer NOT NULL,
    symbol character varying(3) NOT NULL,
    PRIMARY KEY (id)
);

insert into public.symbols (id, symbol) values (nextval('hibernate_sequence'), 'EUR');
insert into public.symbols (id, symbol) values (nextval('hibernate_sequence'), 'HUF');