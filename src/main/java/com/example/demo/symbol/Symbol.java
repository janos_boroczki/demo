package com.example.demo.symbol;

import javax.persistence.Entity;
import javax.persistence.Table;

import org.springframework.data.jpa.domain.AbstractPersistable;

@Entity
@Table(name = "symbols")
public class Symbol extends AbstractPersistable<Integer> {

	private String symbol;

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	@Override
	public String toString() {
		return "Symbol [id=" + getId() + ", symbol=" + symbol + "]";
	}


}
