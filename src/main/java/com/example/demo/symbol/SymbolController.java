package com.example.demo.symbol;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/symbols")
public class SymbolController {

	@Autowired
	private SymbolRepository symbolRepository;

	@GetMapping
	public List<String> getSymbols() {
		return symbolRepository.findAll(Sort.by("symbol"))
				.stream()
				.map(Symbol::getSymbol)
				.collect(Collectors.toList());
	}

}
