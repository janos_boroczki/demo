package com.example.demo.symbol;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

public interface SymbolRepository extends JpaRepository<Symbol, Integer> {

	Optional<Symbol> findBySymbol(String symbol);
	
}
