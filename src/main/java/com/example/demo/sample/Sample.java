package com.example.demo.sample;

import javax.persistence.Entity;
import javax.persistence.Table;

import org.springframework.data.jpa.domain.AbstractPersistable;

@Entity
@Table(name = "sample")
public class Sample extends AbstractPersistable<Integer> {

	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Sample [name=" + name + ", id=" + getId() + "]";
	}

}
