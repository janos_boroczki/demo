package com.example.demo.sample;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/sample")
public class SampleController {

	@Autowired
	private SampleRepository sampleRepository;
	
	@PostMapping
	public void create(@RequestBody Sample sample) {
		sampleRepository.save(sample);
	}
	
	@GetMapping
	public List<Sample> all() {
		return sampleRepository.findAll();
	}
	
}
