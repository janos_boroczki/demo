package com.example.demo.change;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ChangeRateRepository extends JpaRepository<ChangeRate, Integer> {

	Optional<ChangeRate> findByFromSymbol_SymbolAndToSymbol_Symbol(String fromSymbol, String toSymbol);
}
