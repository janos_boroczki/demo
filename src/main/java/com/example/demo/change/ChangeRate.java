package com.example.demo.change;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.data.jpa.domain.AbstractPersistable;

import com.example.demo.symbol.Symbol;

@Entity
@Table(name = "change_rates")
public class ChangeRate extends AbstractPersistable<Integer> {

	@ManyToOne
	@JoinColumn(name = "from_symbol")
	private Symbol fromSymbol;

	@ManyToOne
	@JoinColumn(name = "to_symbol")
	private Symbol toSymbol;

	private BigDecimal rate;

	public ChangeRate() {
		super();
	}

	public ChangeRate(Symbol fromSymbol, Symbol toSymbol, BigDecimal rate) {
		super();
		this.fromSymbol = fromSymbol;
		this.toSymbol = toSymbol;
		this.rate = rate;
	}

	public Symbol getFromSymbol() {
		return fromSymbol;
	}

	public void setFromSymbol(Symbol fromSymbol) {
		this.fromSymbol = fromSymbol;
	}

	public Symbol getToSymbol() {
		return toSymbol;
	}

	public void setToSymbol(Symbol toSymbol) {
		this.toSymbol = toSymbol;
	}

	public BigDecimal getRate() {
		return rate;
	}

	public void setRate(BigDecimal rate) {
		this.rate = rate;
	}

	@Override
	public String toString() {
		return "ChangeRate [id=" + getId() + ", " + "fromSymbol=" + fromSymbol + ", toSymbol=" + toSymbol + ", rate=" + rate + "]";
	}
	
	

}
