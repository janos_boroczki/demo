package com.example.demo.change;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.example.demo.symbol.Symbol;
import com.example.demo.symbol.SymbolRepository;

@RestController
@RequestMapping("/api")
public class ExchangeController {

	@Autowired
	private ChangeRateRepository changeRateRepository;
	
	@Autowired
	private SymbolRepository symbolRepository;
	
	@GetMapping("/convert")
	public BigDecimal convert(@RequestParam String from, @RequestParam String to, @RequestParam BigDecimal amount) {
		BigDecimal result = changeRateRepository.findByFromSymbol_SymbolAndToSymbol_Symbol(from, to)
				.map(ChangeRate::getRate)
				.map(rate -> amount.multiply(rate))
				.orElse(changeRateRepository.findByFromSymbol_SymbolAndToSymbol_Symbol(to, from)
						.map(ChangeRate::getRate)
						.map(rate -> amount.divide(rate, 2, RoundingMode.HALF_UP))
						.orElse(null));
		
		if (result == null) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		}
		
		return result;
		
	}
	
	@PostMapping("/change-rate")
	public void createChangeRate(@RequestBody CreateChangeRateRequest createChangeRateRequest) {
		Optional<ChangeRate> existingChangeRate = changeRateRepository.findByFromSymbol_SymbolAndToSymbol_Symbol(createChangeRateRequest.getFrom().name(), createChangeRateRequest.getTo().name());
		
		if (existingChangeRate.isPresent()) {
			throw new ResponseStatusException(HttpStatus.CONFLICT);
		}

		Optional<Symbol> from = symbolRepository.findBySymbol(createChangeRateRequest.getFrom().name());
		
		Optional<Symbol> to = symbolRepository.findBySymbol(createChangeRateRequest.getTo().name());
		
		if (from.isEmpty() || to.isEmpty()) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		}
		
		ChangeRate changeRate = new ChangeRate(from.get(), to.get(), createChangeRateRequest.getRate());
		
		changeRateRepository.save(changeRate);
	}
	
}
