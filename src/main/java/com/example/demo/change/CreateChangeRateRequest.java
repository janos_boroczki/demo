package com.example.demo.change;

import java.math.BigDecimal;

public class CreateChangeRateRequest {

	public enum Currency {
		HUF, EUR, USD, CHF, GBP, CNY;
	}

	private Currency from;

	private Currency to;

	private BigDecimal rate;

	public Currency getFrom() {
		return from;
	}

	public void setFrom(Currency from) {
		this.from = from;
	}

	public Currency getTo() {
		return to;
	}

	public void setTo(Currency to) {
		this.to = to;
	}

	public BigDecimal getRate() {
		return rate;
	}

	public void setRate(BigDecimal rate) {
		this.rate = rate;
	}

	@Override
	public String toString() {
		return "CreateChaneRateRequest [from=" + from + ", to=" + to + ", rate=" + rate + "]";
	}

}
