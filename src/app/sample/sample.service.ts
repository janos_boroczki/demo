import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Sample } from './sample';

@Injectable({
	providedIn: 'root'
})
export class SampleService {

	private BASE_URL: string = '/api/sample';

	private samples: Subject<Sample[]> = new Subject<Sample[]>();

	constructor(private http: HttpClient) { }

	public getSamples(): Observable<Sample[]> {
		this.http.get(this.BASE_URL)
				.pipe(map(samples => <Sample[]> samples))
				.subscribe((samples: Sample[]) => this.samples.next(samples));
				
		return this.samples;
	}
	
	public saveSample(sample: Sample): void {
		this.http.post(this.BASE_URL, sample)
				.subscribe(() => this.getSamples());
	}

}
