import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Sample } from '../sample';
import { SampleService } from '../sample.service';

@Component({
	selector: 'sample-list',
	templateUrl: './sample-list.component.html',
	styleUrls: ['./sample-list.component.scss']
})
export class SampleListComponent implements OnInit {

	public samples: Observable<Sample[]> = new Observable<Sample[]>();

	constructor(private sampleService: SampleService) { }

	ngOnInit(): void {
		this.samples = this.sampleService.getSamples();
	}

}
