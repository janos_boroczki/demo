import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { ConvertRequest } from './convert-request';
import { map } from 'rxjs/operators';

@Injectable({
	providedIn: 'root'
})
export class ExchangeService {

	private symbols: Subject<string[]> = new Subject<string[]>();

	constructor(private http: HttpClient) { }

	public getSymbols(): Observable<string[]> {
		this.http.get('/api/symbols')
					.pipe(map(result => <string[]> result))
					.subscribe((symbols: string[]) => this.symbols.next(symbols));
		
		return this.symbols;
	}
	
	public convert(convertRequest: ConvertRequest): Observable<number> {
		
		let params: HttpParams = new HttpParams().set('from', convertRequest.from)
												.set('to', convertRequest.to)
												.set('amount', convertRequest.amount);
		
		const options = { params: params };
		
		return this.http.get('/api/convert', options).pipe(map(result => <number> result));
	}

}
