import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { ExchangeService } from './exchange.service';

@Component({
	selector: 'exchange',
	templateUrl: './exchange.component.html',
	styleUrls: ['./exchange.component.scss']
})
export class ExchangeComponent implements OnInit {

	public symbols: Observable<string[]> = new Observable<string[]>(); 

	public exchangeForm: FormGroup= new FormGroup({
		amount: new FormControl('', [Validators.required]),
		from: new FormControl('', [Validators.required]),
		to: new FormControl('', [Validators.required])
	});
	
	public fromSymbol: string = '';
	
	public toSymbol: string = '';
	
	public fromAmount: number = 0;
	
	public toAmount: number = 0;

	constructor(private exchangeService: ExchangeService) { }

	ngOnInit(): void {
		this.symbols = this.exchangeService.getSymbols();
	}
	
	convert(): void {
		if (this.exchangeForm.invalid) {
			this.exchangeForm.markAllAsTouched();
		} else {
			this.fromSymbol = this.exchangeForm.controls['from'].value;
			this.toSymbol = this.exchangeForm.controls['to'].value;
			this.fromAmount = this.exchangeForm.controls['amount'].value;
			
			this.exchangeService.convert({
				from: this.fromSymbol,
				to: this.toSymbol,
				amount: this.fromAmount
			})
				.subscribe((result: number) => this.toAmount = result);
			
			this.exchangeForm.reset();
		}
	}

}
